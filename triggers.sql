CREATE TABLE emp
 (  empname text,
    salary integer,
    last_date timestamp,
    last_user text
);
--creacio de la funcio que cridara el trigger
CREATE FUNCTION emp_nom_upper() RETURNS trigger 
AS $emp_stamp$
   	BEGIN
		NEW.empname = upper(NEW.empname);
		RETURN NEW;
	END;
 $emp_stamp$ 
LANGUAGE plpgsql;
--creacio del trigger
CREATE TRIGGER emp_stamp BEFORE INSERT OR UPDATE ON emp
    FOR EACH ROW EXECUTE PROCEDURE emp_nom_upper();
--aquest trigger canvia a majuscules el empname
INSERT INTO emp VALUES ('Sara Plans Tries', 1000);
SELECT * FROM emp;

CREATE OR REPLACE FUNCTION emp_stamp() RETURNS trigger AS $emp_stamp$
    BEGIN
        IF NEW.empname IS NULL THEN
            RETURN NULL;
        END IF;
        IF NEW.salary IS NULL THEN
            RETURN NULL;
        END IF;
        IF NEW.salary < 0 THEN
            RETURN NULL;
        END IF;

        NEW.last_date := current_timestamp;
        NEW.last_user := current_user;
        RETURN NEW;
    END;
$emp_stamp$ LANGUAGE plpgsql;
--controla abans del insert que el nom i el salari no siguin null
CREATE TRIGGER emp_stamp BEFORE INSERT OR UPDATE ON emp
    FOR EACH ROW EXECUTE PROCEDURE emp_stamp();
--per provar:
INSERT INTO emp VALUES ('Carles Saus Blanc', 3000);
INSERT INTO emp VALUES ('Carles Saus Blanc', -3000);
INSERT INTO emp VALUES ('Carles Saus Blanc');
INSERT INTO emp VALUES ('');

CREATE FUNCTION emp_no_esborrar() RETURNS trigger 
AS $emp_stamp$
   	BEGIN
		IF OLD.salary > 1000 THEN
			RETURN OLD;
		ELSE
			RETURN NULL;
		END IF;
	END;
 $emp_stamp$ 
LANGUAGE plpgsql;

CREATE TRIGGER emp_stamp BEFORE DELETE ON emp
    FOR EACH ROW EXECUTE PROCEDURE emp_no_esborrar();
    
DELETE FROM emp;

SELECT * FROM emp;


--funcio auditar inserts,delete,update
create function emp_auditar() returns trigger as $emp_stamp$
begin
if (TG_OP = 'DELETE') THEN
insert into emp_audit select 'D', now(), user, old.empname,old.salary;
return old;
elseif (TG_OP = 'UPDATE') THEN
insert into emp_audit select 'U', now(), user, old.empname,old.salary;
return new;
elseif (TG_OP = 'INSERT') THEN
insert into emp_audit select 'I', now(), user, new.empname,new.salary;
return new;
end if;
return null;
end;
$emp_stamp$ language plpgsql;

create trigger emp_auditar before insert or update or delete on emp for each row
execute procedure emp_auditar();


--crear trigger
create function analitica_acabada() returns trigger as $$
declare
	sql1 text := 11;
	reg record;
	id_provatecnica int;
	id_pacient int := 0;
	res text := '';
	trobat int :=0;
begin
	sql1 := 'select * from resultats where resultats.idanalitica = NEW.idanalitica and (resultats='' or resultats is NULL);';
	trobat := 0;
	for reg in EXECUTE(sql1) LOOP
		trobat = 1
	END LOOP;
	return trobat;
end;
$$ language plpgsql;

create or replace function analitica_acabada(param_idanalitica int) returns int as $$
declare
	sql1 text := 11;
	reg record;
	id_provatecnica int;
	id_pacient int := 0;
	res text := '';
	trobat int :=0;
begin
	sql1 := 'select * from resultats where resultats.idanalitica = ' || param_idanalitica || 'and (resultats='' or resultats is NULL);';
	raise notice 'sentencia_sql : %' , sql1;
	trobat := 0;
	for reg in EXECUTE(sql1) LOOP
		trobat := 1;
	END LOOP;
	return trobat;
end;
$$ language plpgsql;
--dades al trigger
NEW.idanalitica
select * from resultats where resultats.idanalitica = NEW.idanalitica and (resultats='' or resultats is NULL);
select * from resultats join resultats patologics on resultats_patologics.idresultat=resultats.idresultat where resultatas.idanalitica=new.idanalitica

insert into pacients (idpacient, nom, cognoms, dni, data_naix, sexe, adreca, ciutat, c_postal, telefon, email) 
values (4, 'marta', 'vizcaino', '89657842J', '2003-06-21' ,'F' ,'c/ postal 23' ,'granollers' ,'08402' ,'665588997' ,'mvs@edt.org');
