CREATE TABLE clientes (
    num_clie smallint NOT NULL,
    empresa character varying(20) NOT NULL,
    rep_clie smallint NOT NULL,
    limite_credito numeric(8,2)
);
CREATE TABLE magatzems (
	idMagatzem smallint UNIQUE
	);
	
CREATE TABLE camions (
	idCamio varchar PRIMARY KEY
	);
	
CREATE TABLE transportistes (
	idTransportista integer PRIMARY KEY,
	);
	
CREATE TABLE productes-magatzems (
	idProducte integer,
	idMagatzem integer FOREIGN KEY
		REFERENCES magatzems idMagatzem,
	stock integer not null,
	CONSTRAINT idProducte_idMagatzem PRIMARY KEY(idMagatzem,idProducte)
);

CREATE TABLE transports (
	idTransport serial PRIMARY KEY,
	idCamio FOREIGN KEY 
		REFERENCES camions idCamio,
	idTransportista FOREIGN KEY
		REFERENCES transportistes idTransportista,
);

CREATE TABLE 
